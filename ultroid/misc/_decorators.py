from .. import *
import re, inspect, sys
from telethon import *
from ..dB.database import Var
from pathlib import Path
from traceback import format_exc
from time import gmtime, strftime
from asyncio import create_subprocess_shell as asyncsubshell, subprocess as asyncsub
from os import remove
from sys import *

hndlr = Var.HNDLR

# decorator
def ultroid_cmd(**args):
    args["func"] = lambda e: e.via_bot_id is None
    stack = inspect.stack()
    previous_stack_frame = stack[1]
    file_test = Path(previous_stack_frame.filename)
    file_test = file_test.stem.replace(".py", "")
    pattern = args.get("pattern", None)
    allow_sudo = args.get("allow_sudo", False)
    groups_only = args.get("groups_only", False)
    disable_edited = args.get('disable_edited', True)
    disable_errors = args.get('disable_errors', False)
    trigger_on_fwd = args.get('trigger_on_fwd', False)
    trigger_on_inline = args.get('trigger_on_inline', False)
    args["outgoing"] = True
       
    if allow_sudo:
        args["from_users"] = list(Var.SUDO_USERS)
        args["incoming"] = True
        del args["allow_sudo"]

    elif "incoming" in args and not args["incoming"]:
        args["outgoing"] = True
        
    if pattern is not None:
    	if pattern.startswith("\#"):
            args["pattern"] = re.compile(pattern)
    	else:
    	    args["pattern"] = re.compile(hndlr + pattern)
    					
    args["blacklist_chats"] = True
    black_list_chats = list(Var.BLACKLIST_CHAT)
    if len(black_list_chats) > 0:
    	args["chats"] = black_list_chats

    # check if the plugin should allow edited updates
    allow_edited_updates = False
    if "allow_edited_updates" in args and args["allow_edited_updates"]:
        allow_edited_updates = args["allow_edited_updates"]
        del args["allow_edited_updates"]
    if "trigger_on_inline" in args:
        del args['trigger_on_inline']
    if "disable_edited" in args:
        del args['disable_edited']
    if "groups_only" in args:
        del args['groups_only']
    if "disable_errors" in args:
        del args['disable_errors']
    if "trigger_on_fwd" in args:
        del args['trigger_on_fwd']
    # check if the plugin should listen for outgoing 'messages'
    is_message_enabled = True

    def decorator(func):
        async def wrapper(ult):
            if Var.LOG_CHANNEL:
                send_to = Var.LOG_CHANNEL
            if not trigger_on_fwd and ult.fwd_from:
                return
            if ult.via_bot_id and not trigger_on_inline:
                return
            if disable_errors:
                return 
            if groups_only and not ult.is_group:
                await ult.respond("`I don't think this is a group.`")
                return            
            try:
                await func(ult)
            except events.StopPropagation:
                raise events.StopPropagation
            except KeyboardInterrupt:
                pass
            except BaseException as e:
                LOGS.exception(e)
                if not disable_errors:
                    date = strftime("%Y-%m-%d %H:%M:%S", gmtime())

                    text = "**Sorry, I encountered an error!**\n"
                    link = "#todo"
                    text += "If you want you can report it"
                    text += f"- just forward this message to {link}.\n"
                    text += "I won't log anything except the fact of error and date\n"

                    ftext = "\nDisclaimer:\nThis file uploaded ONLY here, "
                    ftext += "we logged only fact of error and date, "
                    ftext += "we respect your privacy, "
                    ftext += "you may not report this error if you've "
                    ftext += "any confidential data here, no one will see your data "
                    ftext += "if you choose not to do so.\n\n"
                    ftext += "--------START ULTROID CRASH LOG--------"
                    ftext += "\nDate: " + date
                    ftext += "\nGroup ID: " + str(ult.chat_id)
                    ftext += "\nSender ID: " + str(ult.sender_id)
                    ftext += "\n\nEvent Trigger:\n"
                    ftext += str(ult.text)
                    ftext += "\n\nTraceback info:\n"
                    ftext += str(format_exc())
                    ftext += "\n\nError text:\n"
                    ftext += str(sys.exc_info()[1])
                    ftext += "\n\n--------END ULTROID CRASH LOG--------"

                    command = "git log --pretty=format:\"%an: %s\" -5"

                    ftext += "\n\n\nLast 5 commits:\n"

                    process = await asyncsubshell(command,
                                                  stdout=asyncsub.PIPE,
                                                  stderr=asyncsub.PIPE)
                    stdout, stderr = await process.communicate()
                    result = str(stdout.decode().strip()) \
                        + str(stderr.decode().strip())

                    ftext += result

                    file = open("crash.log", "w+")
                    file.write(ftext)
                    file.close()

                    if Var.LOG_CHANNEL:
                        await ult.client.send_file(
                            send_to,
                            "crash.log",
                            caption=text,
                        )
                    else:
                        await ult.client.send_file(
                            ult.chat_id,
                            "crash.log",
                            caption=text,
                        )

                    remove("crash.log")
        if not disable_edited:
            ultroid_bot.asst.add_event_handler(wrapper, events.MessageEdited(**args))
        ultroid_bot.asst.add_event_handler(wrapper, events.NewMessage(**args))
        return wrapper
    return decorator
