# we save the vars here // remove this line later
import os

ENV = bool(os.environ.get("ENV", False))

class Var(object):
    API_ID = os.environ.get("API_ID", None)
    if API_ID:
        API_ID = int(API_ID)
    API_HASH = os.environ.get("API_HASH", None)
    BOT_TOKEN = os.environ.get("BOT_TOKEN", None)
    BOT_USERNAME = os.environ.get("BOT_USERNAME", None)
    SESSION = os.environ.get("SESSION", None)
    DB_URI = os.environ.get("DATABASE_URL", None)
    HNDLR = os.environ.get("HNDLR", None)
    LOG_CHANNEL = os.environ.get("LOG_CHANNEL", None)
    if LOG_CHANNEL:
        LOG_CHANNEL = int(LOG_CHANNEL)
    SUDO_USERS = set(int(x) for x in os.environ.get("SUDO_USERS", "").split())
    BLACKLIST_CHAT = set(int(x) for x in os.environ.get("BLACKLIST_CHAT", "").split())
