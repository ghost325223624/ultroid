from telethon import events

@ultroid.on(events.NewMessage(outgoing=True, pattern=f"{hndlr}set ?(.*)"))
async def _(event):
    ok = event.pattern_match.group(1)
    await event.reply(f"Setting var TEMP from {TEMP} to {ok}")
    TEMP = f"{ok}"
    
