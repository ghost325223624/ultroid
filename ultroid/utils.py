from . import *
import re, inspect, sys, functools
from telethon import *
from .dB.database import Var
from pathlib import Path
from traceback import format_exc
from time import gmtime, strftime
from asyncio import create_subprocess_shell as asyncsubshell, subprocess as asyncsub
from os import remove
from sys import *

# for plugins
def load_plugins(plugin_name):
    if plugin_name.startswith("__"):
        pass
    elif plugin_name.endswith("_"):
        import importlib
        from pathlib import Path
        path = Path(f"ultroid/plugins/{plugin_name}.py")
        name = "ultroid.plugins.{}".format(plugin_name)
        spec = importlib.util.spec_from_file_location(name, path)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
    else:
        import importlib, sys
        from .misc._assistant import owner, asst_cmd
        from .misc._decorators import ultroid_cmd
        from pathlib import Path
        from .dB.database import Var
        path = Path(f"ultroid/plugins/{plugin_name}.py")
        name = "ultroid.plugins.{}".format(plugin_name)
        spec = importlib.util.spec_from_file_location(name, path)
        mod = importlib.util.module_from_spec(spec)
        mod.asst = ultroid_bot.asst
        mod.ultroid = ultroid_bot
        mod.owner = owner()
        mod.hndlr = Var.HNDLR
        mod.Var = Var
        mod.asst_cmd = asst_cmd
        mod.ultroid_cmd = ultroid_cmd
        spec.loader.exec_module(mod)
        sys.modules["ultroid.plugins." + plugin_name] = mod

# for assistant
def load_assistant(plugin_name):
    if plugin_name.startswith("__"):
        pass
    elif plugin_name.endswith("_"):
        import importlib
        from pathlib import Path
        path = Path(f"ultroid/assistant/{plugin_name}.py")
        name = "ultroid.assistant.{}".format(plugin_name)
        spec = importlib.util.spec_from_file_location(name, path)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
    else:
        import importlib, sys
        from .misc._assistant import owner, asst_cmd
        from .misc._decorators import ultroid_cmd
        from pathlib import Path
        from .dB.database import Var
        path = Path(f"ultroid/assistant/{plugin_name}.py")
        name = "ultroid.assistant.{}".format(plugin_name)
        spec = importlib.util.spec_from_file_location(name, path)
        mod = importlib.util.module_from_spec(spec)
        mod.ultroid = ultroid_bot
        mod.asst = ultroid_bot.asst
        mod.owner = owner()
        mod.hndlr = Var.HNDLR
        mod.asst_cmd = asst_cmd
        spec.loader.exec_module(mod)
        sys.modules["ultroid.assistant." + plugin_name] = mod
