import functools
from telethon import events
from .. import *

# decorator for assistant
def asst_cmd(dec):
    def ult(func):
        pattern = "^/" + dec    # todo - handlers for assistant?
        asst.add_event_handler(func, events.NewMessage(incoming=True, pattern=pattern))
    return ult
    
# check for owner
def owner():
    def decorator(function):
        @functools.wraps(function)
        async def wrapper(event):
            if event.sender_id == ultroid_bot.uid:
                await function(event)
            else:
                pass
        return wrapper
    return decorator
