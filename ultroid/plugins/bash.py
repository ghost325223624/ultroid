import subprocess, io, asyncio, time, inspect, traceback, sys
from telethon.errors import MessageEmptyError, MessageTooLongError, MessageNotModifiedError
from telethon import events, errors, functions, types

"""
.bash - For Linux cmds
"""


@ultroid.on(events.NewMessage(pattern="{hndlr}bash ?(.*)", incoming=True))
@ultroid.on(events.NewMessage(pattern="{hndlr}bash ?(.*)", outgoing=True))
async def _(event):
    if event.fwd_from:
        return
    DELAY_BETWEEN_EDITS = 0.3
    PROCESS_RUN_TIME = 100
    cmd = event.pattern_match.group(1)
    reply_to_id = event.message.id
    if event.reply_to_msg_id:
        reply_to_id = event.reply_to_msg_id
    start_time = time.time() + PROCESS_RUN_TIME
    process = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    stdout, stderr = await process.communicate()
    e = stderr.decode()
    if not e:
        e = "No Error"
    o = stdout.decode()
    if not o:
        o = "No output"
    else:
        _o = o.split("\n")
        o = "`\n".join(_o)
    OUTPUT = f"**• COMMAND:**\n`{cmd}` \n\n**• PID:**\n`{process.pid}`\n\n**• ERROR:** \n`{e}`\n\n**• OUTPUT:**\n`{o}`"
    if len(OUTPUT) > 4095:
        with io.BytesIO(str.encode(OUTPUT)) as out_file:
            out_file.name = "bash.text"
            await event.client.send_file(
                event.chat_id,
                out_file,
                force_document=True,
                allow_cache=False,
                caption=cmd,
                reply_to=reply_to_id
            )
            await event.delete()
    await event.reply(OUTPUT)
    await event.delete()
